

## Space Invader

- Creator: Maximiliano Vallejos

- Technology: 
	- Unity 2019.3.1f1
	- C#

- Features: 
	- Lose condition: lose 3 lives
	- Win condition: kill all invaders
	- Player horizontal move with buttons
	- Player fire with buttons
	- Only one projectile alive per weapon
	- Invaders horizontal and vertical desynchronized move
	- Restart level on win or lose
	- Invader die on player projectile impact
	- Player decrease on live on player projectile impact
	- Blocks intercepts all projectile and reduce size until disappear
	- Show lives on HUD
