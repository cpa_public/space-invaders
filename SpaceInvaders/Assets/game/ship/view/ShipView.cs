using game.weapon;
using UnityEngine;

namespace game.ship.view
{
    public class ShipView : MonoBehaviour
    {
        private Ship _ship;
        [SerializeField]
        private GameObject _projectilePrefab;
        [SerializeField]
        private Transform _projectilePlaceHolder;

        [SerializeField]
        private Transform _projectileContainer;

        private Weapon _weapon;

        public Ship Ship
        {
            get { return _ship; }
        }

        private void Awake()
        {
            _ship = new Ship(0, 500);
        }

        private void Start()
        {
            _weapon = new Weapon(_projectilePrefab, _projectileContainer);
            UpdatePosition();
        }

        private void UpdatePosition()
        {
            transform.localPosition = Vector3.right * _ship.Position;
        }

        private void Update()
        {
            UpdatePosition();
            
            if (!_ship.Alive)
            {
                Destroy(gameObject);   
            }

            if (_ship.ReadyToFire)
            {
                Fire();
            }
        }

        private void Fire()
        {
            _weapon.Fire(_projectilePlaceHolder.position, Vector3.up);
            _ship.FireDone();
        }
        
        private void OnTriggerEnter2D(Collider2D other)
        {
            OnImpact();
        }

        private void OnImpact()
        {
            _ship.OnImpact();
            
        }    
    }
    
}