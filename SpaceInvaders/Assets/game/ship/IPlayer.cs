using UnityEngine;

namespace game.ship
{
    public interface IPlayer
    {
        void Move(Vector2 direction);
        void Fire();
    }
}