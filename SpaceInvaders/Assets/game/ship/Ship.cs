using UnityEngine;

namespace game.ship
{
    public class Ship : IPlayer
    {
        private float _posX;
        private float _speed;
        private float _initPosX;
        private int _lives = 3;
        private bool _readyToFire;

        public bool Alive
        {
            get { return _lives > 0; }
        }

        public Ship(float positionX, float speed)
        {
            _initPosX = positionX;
            _posX = positionX;
            _speed = speed;
        }

        public float Position
        {
            get { return _posX; }
        }

        public int Lives
        {
            get { return _lives; }
        }

        public bool ReadyToFire
        {
            get { return _readyToFire; }
        }

        public void Move(Vector2 direction)
        {
            if (!Alive)
                return;
            _posX += direction.x * _speed;
        }


        public void OnImpact()
        {
            _posX = _initPosX;
            _lives--;
        }

        public void Fire()
        {
            _readyToFire = true;
        }

        public void FireDone()
        {
            _readyToFire = false;
        }
    }
}