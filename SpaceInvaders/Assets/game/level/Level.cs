using System.Collections.Generic;
using System.Linq;
using game.invader;
using game.ship;
using game.ship.view;
using game.UI.controls;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace game.level
{
    public class Level : MonoBehaviour
    {
        [SerializeField] private ShipView _shipView;
        [SerializeField] private ControlsView _controllerView;
        [SerializeField] private TMP_Text _livesText;
        private Ship _ship;
        private bool _gameOver;
        private List<InvaderView> _enemies;

        private float _timeToRestart = 3;

        private void Start()
        {
            _ship = _shipView.Ship;
            _enemies = GetComponentsInChildren<InvaderView>().ToList();
            _enemies.ForEach(e => e.OnImpact += OnEnemyDie);
            _controllerView.Init(_ship);
        }

        private void OnEnemyDie()
        {
            if (_enemies.Any(e => e.IsAlive))
                return;
            
            _gameOver = true;
        }

        private void Update()
        {
            _livesText.text = _ship.Lives.ToString();
            if (!_ship.Alive)
            {
                _gameOver = true;
            }

            if (_gameOver)
            {
                _timeToRestart -= Time.deltaTime;
                if (_timeToRestart <= 0)
                {
                    RestartGame();
                }
            }
        }

        private void RestartGame()
        {
            SceneManager.LoadScene("Main", LoadSceneMode.Single);
        }
    }
}