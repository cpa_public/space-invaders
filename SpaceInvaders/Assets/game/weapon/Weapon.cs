using game.projectile;
using UnityEngine;

namespace game.weapon
{
    public class Weapon
    {
        private GameObject _projectilePrefab;
        private Transform _projectileContainer;
        private bool _canFire;
        private ProjectileView _projectileView;
        private bool _projectileCreated = false;

        public Weapon(GameObject projectilePrefab, Transform projectileContainer)
        {
            _projectilePrefab = projectilePrefab;
            _projectileContainer = projectileContainer;
            _canFire = true;
        }
        
        public void Fire(Vector3 position, Vector3 direction)
        {
            if (!_canFire)
                return;

            _canFire = false;
            
            SpawnProjectile();
            _projectileView.Fire(direction, position);
            _projectileView.OnRemove += ()=>
            {
                OnRemove(_projectileView);
            };
        }

        private void SpawnProjectile()
        {
            if (_projectileCreated)
                return;
            
            _projectileCreated = true;
            var instance = GameObject.Instantiate(_projectilePrefab, _projectileContainer);
            _projectileView = instance.GetComponent<ProjectileView>();
            
        }

        private void OnRemove(ProjectileView projectileView)
        {
            _canFire = true;
            projectileView.Remove();
        }
    }
}