using UnityEngine;

namespace game.weapon
{
    public class AutofiredWeapon : Weapon
    {
        private Transform _firePosition;
        private Vector3 _direction;
        private float _timeToFire;
        private int _probability;
        private const float FIRE_MIN_TIME = 3;

        public AutofiredWeapon(GameObject projectilePrefab, Transform projectileContainer, Transform firePosition, Vector3 direction, int probability) : base(projectilePrefab, projectileContainer)
        {
            _firePosition = firePosition;
            _direction = direction;
            _probability = probability;
            _timeToFire = FIRE_MIN_TIME;
        }

        public void Update()
        {
            _timeToFire -= Time.deltaTime;
            if (ShouldCheckFire())
            {
                _timeToFire = FIRE_MIN_TIME;
                if(CheckFire())
                    Fire(_firePosition.position, _direction);
            }
        }

        private bool ShouldCheckFire()
        {
            return _timeToFire <= 0 ;
        }

        private bool CheckFire()
        {
            return Random.Range(0, 100) < _probability;
        }
    }
}