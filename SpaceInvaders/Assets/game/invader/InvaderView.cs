using System;
using game.weapon;
using UnityEngine;
using Random = UnityEngine.Random;

namespace game.invader
{
    public class InvaderView : MonoBehaviour
    {
        private const float STEP_TIME = 2f;
        private const float SPEED_X = 50;
        private const float SPEED_Y = 100;
        private const int MOVES_COUNT_MAX = 8; 
        private float _timeToMove;
        private float _direction = 1;
        private int _movesCount = 0;
        private float _timeOffset;
        private AutofiredWeapon _weapon;
        [SerializeField]
        private GameObject _projectilePrefab;
        [SerializeField]
        private Transform _projectileContainer;

        [SerializeField]
        private int _fireProbability;

        
        private bool _isAlive = true;
        public bool IsAlive
        {
            get { return _isAlive; }
        }


        private void Start()
        {
            RestartMoveTime();
            _timeOffset = Random.Range(0, 0.02f);
            _weapon = new AutofiredWeapon(_projectilePrefab, _projectileContainer, transform, Vector3.down, _fireProbability);
            
        }

        private void RestartMoveTime()
        {
            _timeToMove = STEP_TIME;
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            gameObject.SetActive(false);
            _isAlive = false;
            if (OnImpact != null) OnImpact();
        }

        private void Update()
        {
            UpdateMove();
            _weapon.Update();
        }

        private void UpdateMove()
        {
            _timeToMove -= Time.deltaTime;
            if (ShouldMoveX())
            {
                MoveX();
                RestartMoveTime();
                _movesCount++;
            }

            if (ShouldMoveY())
            {
                MoveY();
                ReversXDirection();
                RestartMovesCount();
            }
        }

        private void ReversXDirection()
        {
            _direction *= -1;
        }

        private bool ShouldMoveY()
        {
            return _movesCount >= MOVES_COUNT_MAX;
        }

        private bool ShouldMoveX()
        {
            return _timeToMove - _timeOffset <= 0;
        }

        private void RestartMovesCount()
        {
            _movesCount = 0;
        }

        private void MoveX()
        {
            transform.localPosition += Vector3.right * (_direction * SPEED_X);
        }

        private void MoveY()
        {
            transform.localPosition += Vector3.down * SPEED_Y;
        }

        public event Action OnImpact;
    }
}