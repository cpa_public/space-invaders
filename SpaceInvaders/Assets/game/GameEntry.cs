﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace game
{
    public class GameEntry : MonoBehaviour
    {
        private void Start()
        {
            Play();
        }

        private void Play()
        {
            SceneManager.LoadScene("Game", LoadSceneMode.Additive);
        }
    }
}

