﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace game
{
    public class DebugNavigateToMain : MonoBehaviour
    {
        public void Start()
        {
            if(!Debug.isDebugBuild)
            {
                Destroy(gameObject);
            }

            if (FindObjectOfType<GameEntry>() == null)
            {
                SceneManager.LoadScene("Main", LoadSceneMode.Single);
            }
        }
        
    }
}