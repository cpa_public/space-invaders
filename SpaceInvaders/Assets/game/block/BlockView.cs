﻿using UnityEngine;
using UnityEngine.UI;

public class BlockView : MonoBehaviour
{
    [SerializeField]
    private Image _image;

    private BoxCollider2D _collider;
    private Vector2 _reduce;

    private void Start()
    {
        _collider = GetComponent<BoxCollider2D>();
        _reduce = new Vector2(0, 10);
    }
    
    private void OnTriggerEnter2D(Collider2D other)
    {
        ReduceSprite();
    }

    private void ReduceSprite()
    {
        _image.rectTransform.sizeDelta -= _reduce;
        _collider.size -= _reduce;
    }
}
