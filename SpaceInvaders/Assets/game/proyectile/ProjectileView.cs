﻿using System;
using UnityEngine;

namespace game.projectile
{
    public class ProjectileView : MonoBehaviour
    {
        public event Action OnRemove;
        private float _speed = 750;
        private Vector3 _direction;

        

        private void Update()
        {
            transform.position += _direction * (_speed * Time.deltaTime);
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            if (OnRemove != null) 
                OnRemove();
            
        }

        public void Fire(Vector3 direction, Vector3 position)
        {
            _direction = direction;
            transform.position = position;
            gameObject.SetActive(true);
        }

        public void Remove()
        {
            gameObject.SetActive(false);
        }
    }
}
