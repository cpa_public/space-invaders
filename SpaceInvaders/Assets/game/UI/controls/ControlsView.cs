using System;
using game.ship;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace game.UI.controls
{
    public class ControlsView : MonoBehaviour
    {
        [SerializeField]
        private EventTrigger _leftButton;
        [SerializeField]
        private EventTrigger _rightButton;
        [SerializeField]
        private Button _fireButton;

        private IPlayer _player;
        private bool _leftPressed;
        private bool _rightPressed;

        public void Init(Ship ship)
        {
            _player = ship;
            SetupButtons();
        }

        private void SetupButtons()
        {
            AddTriggerEntry(_rightButton, EventTriggerType.PointerDown, PointerDownRight);
            AddTriggerEntry(_rightButton, EventTriggerType.PointerUp, PointerUpRight);
            AddTriggerEntry(_leftButton, EventTriggerType.PointerDown, PointerDownLeft);
            AddTriggerEntry(_leftButton, EventTriggerType.PointerUp, PointerUpLeft);
            _fireButton.onClick.AddListener(OnFire);
        }

        private void OnFire()
        {
            _player.Fire();
        }

        private void AddTriggerEntry(EventTrigger trigger,  EventTriggerType triggerType, Action onTrigger)
        {
            var entry = new EventTrigger.Entry();
            entry.eventID = triggerType;
            entry.callback.AddListener((data) => { onTrigger(); });
            trigger.triggers.Add(entry);
        }

        public void PointerDownRight()
        {
            _rightPressed = true;
        }
        
        public void PointerDownLeft()
        {
            _leftPressed = true;
        }
        
        public void PointerUpRight()
        {
            _rightPressed = false;
        }
        
        public void PointerUpLeft()
        {
            _leftPressed = false;
        }

        private void FixedUpdate()
        {
            if(_rightPressed)
                _player.Move(Vector2.right * Time.deltaTime);
            if(_leftPressed)
                _player.Move(Vector2.left * Time.deltaTime);
        }
    }
}